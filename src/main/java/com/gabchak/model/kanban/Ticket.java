package com.gabchak.model.kanban;

import com.gabchak.exception.TicketException;

import java.util.StringJoiner;

public class Ticket {
    private Integer id;
    private String name;
    private State state;

    public Ticket(Integer id, String name, State state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    void changeState(State newState) throws TicketException {
        state.changeState(this, newState);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ticket.class.getSimpleName() + "[",
                "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("state=" + state)
                .toString();
    }
}
