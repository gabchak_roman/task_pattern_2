package com.gabchak.model.kanban;

import com.gabchak.exception.TicketException;

import java.util.Set;

public interface State {
    void changeState(Ticket ticket, State newState) throws TicketException;

    Set<State> getAllowedStates();
}
