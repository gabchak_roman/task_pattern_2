package com.gabchak.model.kanban;

import com.gabchak.exception.TicketException;
import com.gabchak.model.kanban.state.ToDoState;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class KanbanBoard {
    private Map<Integer, Ticket> allTickets = new LinkedHashMap<>();
    private AtomicInteger nextTicketId = new AtomicInteger(1);

    public Ticket addTicket(String name) {
        Ticket ticket = new Ticket(nextTicketId.getAndIncrement(), name,
                ToDoState.instance());
        allTickets.put(ticket.getId(), ticket);
        return ticket;
    }

    public Set<State> getTicketAllowedStates(Integer id)
            throws TicketException {
        Ticket ticket = getTicket(id);
        return ticket.getState().getAllowedStates();
    }

    private Ticket getTicket(Integer id) throws TicketException {
        Ticket ticket = allTickets.get(id);
        if (ticket == null) {
            throw new TicketException("Ticket with id '" + id + "' not found.");
        }
        return ticket;
    }

    public void setStateForTicket(State newState, Integer ticketId)
            throws TicketException {
        Ticket ticket = getTicket(ticketId);
        ticket.changeState(newState);
    }

    public Collection<Ticket> getAllTickets() {
        return allTickets.values();
    }
}
