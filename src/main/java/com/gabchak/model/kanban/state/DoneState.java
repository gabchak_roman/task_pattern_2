package com.gabchak.model.kanban.state;

import com.gabchak.model.kanban.State;
import com.gabchak.model.kanban.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DoneState extends BaseState {
    private static final Logger LOG =
            LogManager.getLogger(DoneState.class);
    private static DoneState instance = new DoneState();

    public static DoneState instance() {
        return instance;
    }

    @Override
    protected void actionsBeforeChange(Ticket ticket, State newState) {
        LOG.info("Actions before setting '{}' state for {}", newState, ticket);
    }

    @Override
    protected void applyFor(Ticket ticket) {
        LOG.info("Applying new state '{}' for {}", this, ticket);
    }

    @Override
    public String toString() {
        return "Done";
    }
}
