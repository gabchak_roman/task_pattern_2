package com.gabchak.model.kanban.state;

import com.gabchak.model.kanban.State;
import com.gabchak.model.kanban.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CodeReviewState extends BaseState {
    private static final Logger LOG =
            LogManager.getLogger(CodeReviewState.class);

    private static CodeReviewState instance = new CodeReviewState();

    static {
        instance().allowedNextStates.add(InProgressState.instance());
        instance().allowedNextStates.add(DoneState.instance());
    }

    public static CodeReviewState instance() {
        return instance;
    }

    @Override
    protected void actionsBeforeChange(Ticket ticket, State newState) {
        LOG.info("Actions before setting '{}' state for {}", newState, ticket);
    }

    @Override
    protected void applyFor(Ticket ticket) {
        LOG.info("Applying new state '{}' for {}", this, ticket);
    }

    @Override
    public String toString() {
        return "Code Review";
    }
}
