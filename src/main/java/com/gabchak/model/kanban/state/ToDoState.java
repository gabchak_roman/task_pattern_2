package com.gabchak.model.kanban.state;

import com.gabchak.model.kanban.State;
import com.gabchak.model.kanban.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ToDoState extends BaseState {
    private static final Logger LOG =
            LogManager.getLogger(ToDoState.class);
    private static ToDoState instance = new ToDoState();

    static {
        instance().allowedNextStates.add(InProgressState.instance());
    }

    public static ToDoState instance() {
        return instance;
    }

    @Override
    protected void actionsBeforeChange(Ticket ticket, State newState) {
        LOG.info("Actions before setting '{}' state for {}", newState, ticket);
    }

    @Override
    protected void applyFor(Ticket ticket) {
        LOG.info("Applying new state '{}' for {}", this, ticket);
    }

    @Override
    public String toString() {
        return "ToDo";
    }
}
