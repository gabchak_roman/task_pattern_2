package com.gabchak.model.kanban.state;

import com.gabchak.exception.TicketException;
import com.gabchak.model.kanban.State;
import com.gabchak.model.kanban.Ticket;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseState implements State {
    Set<State> allowedNextStates = new HashSet<>();

    public Set<State> getAllowedStates() {
        return allowedNextStates;
    }

    @Override
    public void changeState(Ticket ticket, State newState) throws
            TicketException {
        checkNewStateIsAllowed(newState);
        actionsBeforeChange(ticket, newState);
        ticket.setState(newState);
        if (newState instanceof BaseState) {
            ((BaseState) newState).applyFor(ticket);
        }
    }

    protected abstract void actionsBeforeChange(Ticket ticket, State newState);

    protected abstract void applyFor(Ticket ticket);

    private void checkNewStateIsAllowed(State newState) throws TicketException {
        if (!allowedNextStates.contains(newState)) {
            throw new TicketException("The state '" + newState + "' is not allowed");
        }
    }
}
