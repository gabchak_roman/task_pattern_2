package com.gabchak.model.menu;

import java.io.IOException;

public interface MenuItem {

    String getTitle();

    String getKey();

    void execute() throws IOException;

}
