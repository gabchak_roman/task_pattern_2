package com.gabchak.model.menu.items;

import com.gabchak.exception.TicketException;
import com.gabchak.model.kanban.KanbanBoard;
import com.gabchak.model.kanban.State;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;

public class ChangeTicketState extends AbstractMenuItem {
    private static final Logger LOG =
            LogManager.getLogger(ChangeTicketState.class);

    private static final String NAME = "Change ticket state";
    private static final String DETAILS = "";
    private final ConsoleReader consoleReader;

    private KanbanBoard board;

    public ChangeTicketState(String key, View view, KanbanBoard board,
                             ConsoleReader consoleReader) {
        super(key, format(TITLE_FORMAT, NAME, DETAILS), view);
        this.board = board;
        this.consoleReader = consoleReader;
    }

    @Override
    public void execute() {
        new ViewBoard(null, view, board).execute();
        System.out.println("\n".repeat(2));
        view.printMessage("Enter ticket id: ");
        Integer ticketId = null;
        try {
            ticketId = Integer.parseInt(consoleReader.readLine());
        } catch (NumberFormatException e) {
            LOG.warn("Wrong input!");
        }
        try {
            Set<State> ticketAllowedStates =
                    board.getTicketAllowedStates(ticketId);
            if (ticketAllowedStates.size() == 0) {
                view.printMessageRed("There is no allowed states for the ticket");
                return;
            }

            view.printMessageRed("Allowed ticket states: ");

            Map<Integer, State> states = new HashMap<>();
            int i = 1;
            for (State state : ticketAllowedStates) {
                view.printMessageRed(i + " " + state);
                states.put(i++, state);
            }
            view.printMessageRed("select state:");

            Integer stateId;
            stateId = Integer.parseInt(consoleReader.readLine());
            State state = states.get(stateId);
            if (state == null) {
                view.printMessageRed("State #" + stateId + " is not in a list");
            } else {
                board.setStateForTicket(state, ticketId);
                view.printMessageRed("State was changed to " + state);
            }
        } catch (TicketException e) {
            view.printMessageRed("Can't change ticket state. " + e.getMessage());
        }
    }
}
