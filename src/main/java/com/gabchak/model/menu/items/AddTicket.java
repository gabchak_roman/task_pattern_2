package com.gabchak.model.menu.items;

import com.gabchak.model.kanban.KanbanBoard;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.commons.lang3.StringUtils;

import static java.lang.String.format;

public class AddTicket extends AbstractMenuItem {
    private static final String NAME = "Add new ticket";
    private static final String DETAILS = "";
    private final ConsoleReader consoleReader;

    private KanbanBoard board;

    public AddTicket(String key, View view, KanbanBoard board,
                     ConsoleReader consoleReader) {
        super(key, format(TITLE_FORMAT, NAME, DETAILS), view);
        this.board = board;
        this.consoleReader = consoleReader;
    }

    @Override
    public void execute() {
        String ticketName;
        do {
            view.printMessage("Enter ticket name: ");
            ticketName = consoleReader.readLine();
        } while (StringUtils.isBlank(ticketName));
        board.addTicket(ticketName);
    }
}
