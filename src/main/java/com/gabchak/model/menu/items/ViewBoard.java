package com.gabchak.model.menu.items;

import com.gabchak.model.kanban.KanbanBoard;
import com.gabchak.model.kanban.Ticket;
import com.gabchak.view.View;

import java.util.Collection;

import static java.lang.String.format;

public class ViewBoard extends AbstractMenuItem {

    private static final String NAME = "View Kanban board";
    private static final String DETAILS = "";

    private KanbanBoard board;

    public ViewBoard(String key, View view, KanbanBoard board) {
        super(key, format(TITLE_FORMAT, NAME, DETAILS), view);
        this.board = board;
    }

    @Override
    public void execute() {
        Collection<Ticket> tickets = board.getAllTickets();
        if (tickets.size() == 0) {
            view.printMessageRed("The board is empty. Please add a new ticket.");
        } else {
            view.printMessageRed("All tickets on the board:");
            view.printTickets(tickets);
        }
    }
}
