package com.gabchak.model.menu.items;

import com.gabchak.model.menu.MenuItem;
import com.gabchak.view.View;

import java.io.IOException;

public abstract class AbstractMenuItem implements MenuItem {
    static final String TITLE_FORMAT = "%-25s %s";
    protected View view;
    private String title;
    private String key;

    protected AbstractMenuItem(String key, String title, View view) {
        this.key = key;
        this.title = title;
        this.view = view;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public abstract void execute() throws IOException;
}
