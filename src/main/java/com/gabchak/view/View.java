package com.gabchak.view;

import com.gabchak.model.kanban.Ticket;

import java.util.Collection;

public interface View {

    void printMessage(String message);

    void printMessageRed(String message);

    void printTickets(Collection<Ticket> ticket);
}
