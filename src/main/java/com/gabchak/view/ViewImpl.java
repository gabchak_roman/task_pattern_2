package com.gabchak.view;

import com.gabchak.model.kanban.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;

public class ViewImpl implements View {
    private static final Logger LOG =
            LogManager.getLogger(ViewImpl.class);

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public final void printMessage(final String message) {
        LOG.info(ANSI_BLACK + message + ANSI_RESET);
    }

    @Override
    public final void printMessageRed(final String message) {
        LOG.info(ANSI_RED + message + ANSI_RESET);
    }

    @Override
    public void printTickets(Collection<Ticket> tickets) {
        String ticketsHeader = String.format("%3s %-50s %-15s", "ID", "Name", "State");
        LOG.info(ANSI_BLACK + ticketsHeader + ANSI_RESET);
        tickets.forEach(t -> {
            String formattedTicket = String.format("%3d %-50s %-15s", t.getId(), t.getName(), t.getState());
            LOG.info(ANSI_YELLOW + formattedTicket + ANSI_RESET);
        });
    }
}

