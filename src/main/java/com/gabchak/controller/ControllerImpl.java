package com.gabchak.controller;

import com.gabchak.model.kanban.KanbanBoard;
import com.gabchak.model.kanban.Ticket;
import com.gabchak.model.kanban.state.CodeReviewState;
import com.gabchak.model.kanban.state.DoneState;
import com.gabchak.model.kanban.state.InProgressState;
import com.gabchak.model.kanban.state.ToDoState;
import com.gabchak.model.menu.MenuItem;
import com.gabchak.model.menu.items.*;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ControllerImpl implements Controller {
    private static final Logger LOG =
            LogManager.getLogger(ControllerImpl.class);

    private View view;
    private ConsoleReader consoleReader;
    private Map<String, MenuItem> menu;
    private MenuItem notExistingItem;
    private KanbanBoard board;

    public ControllerImpl(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.board = new KanbanBoard();
        initializeSomeTickets();
    }

    private void initializeSomeTickets() {
        Ticket ticket = board.addTicket("Implement task 1");
        ticket.setState(DoneState.instance());
        ticket = board.addTicket("Implement task 2");
        ticket.setState(DoneState.instance());


        ticket = board.addTicket("Implement task 3");
        ticket.setState(CodeReviewState.instance());

        ticket = board.addTicket("Implement task 4");
        ticket.setState(InProgressState.instance());
        ticket = board.addTicket("Implement task 5");
        ticket.setState(InProgressState.instance());


        ticket = board.addTicket("Implement task 5");
        ticket.setState(ToDoState.instance());
        ticket = board.addTicket("Implement task 6");
        ticket.setState(ToDoState.instance());
    }

    public void start() {
        buildMenu();
        startMainLoop();
    }

    private void buildMenu() {
        menu = new LinkedHashMap<>();

        List<MenuItem> items = new ArrayList<>();
        items.add(new ViewBoard("1", view, board));
        items.add(new AddTicket("2", view, board, consoleReader));
        items.add(new ChangeTicketState("3", view, board, consoleReader));
        items.add(new AbstractMenuItem("Q", "Exit", view) {

            @Override
            public void execute() {
                view.printMessage("Bye");
                System.exit(0);
            }
        });

        notExistingItem = new DummyItem(view, "No such menu item.");

        items.forEach(i -> menu.put(i.getKey(), i));
    }

    private void startMainLoop() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select buildMenu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            cleanConsole();
            try {
                menu.getOrDefault(keyMenu, notExistingItem).execute();
            } catch (IOException e) {
                LOG.error("Can't execute menu item {}.", keyMenu, e);
            }
        } while (true);
    }

    private void outputMenu() {
        System.out.println();
        view.printMessage(" MENU:");
        menu.values().forEach(item -> view.printMessage(
                format(" %s - %s", item.getKey(), item.getTitle())));
    }

    private void cleanConsole() {
        System.out.println("\n".repeat(30));
    }
}